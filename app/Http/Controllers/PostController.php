<?php

namespace App\Http\Controllers;

use App\Like;
use App\Post;
use Exception;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['index']]);
    }

    public function index(){

    }

    public function store(Request $request){

        $rules = [
            'post'   => 'required',
            'photo'  => 'required|mimes:jpeg,jpg,png|max:2028'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $validator->getMessageBag()->add('is_post_status', 'Edit Form Is Not valid');
            alert()->error('Gagal Input !!', 'Error');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = auth()->user();

        $data = new Post;
        $data->post = $request->post;
        $data->user_id = $user->id;

        try {

            $uploaded_cover = $request->file('photo');
            $extension = $uploaded_cover->getClientOriginalExtension();

            $filename = md5(time()) . '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
            $uploaded_cover->move($destinationPath, $filename);

            $data->poto = $filename;
            $data->save();
            alert()->success('Data Berhasil diupload !!', 'Sukses');

        } catch (\Exception $e) {
            alert()->error($e->getMessage(), 'Error');
        }


        return redirect()->back();
    }

    public function destroy(Request $request) {
        // return $request->all();

        $data = Post::find($request->id_post);

        if(!empty($data)) {
            $poto = $data->poto;


            Like::query()->where('post_id',$request->id_post)->delete();
            Comment::query()->where('post_id',$request->id_post)->delete();

            $data->delete();

            $filepath = public_path() . DIRECTORY_SEPARATOR . 'img'
                . DIRECTORY_SEPARATOR . $poto;

            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
            alert()->success('Data Berhasil dihapus!!', 'Sukses');
        } else {
            alert()->error('Gagal', 'Error');
        }

        return redirect()->back();
    }
}
