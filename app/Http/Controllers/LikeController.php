<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function like(Request $request)
    {

        $user = auth()->user();
        $post_id = $request->idpost;

        $checkLike = Like::query()->where('post_id',$post_id)->where('user_id',$user->id)->first();

        if(!empty($checkLike)) {
            $checkLike->delete();
            alert()->success('Unlike Berhasil', 'Sukses');
        }
        else {
            Like::create([
                'post_id' => $post_id,
                'user_id' => $user->id
            ]);

            alert()->success('Like Berhasil', 'Sukses');
        }
        return redirect()->back();
    }
}
