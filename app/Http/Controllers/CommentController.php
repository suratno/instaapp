<?php

namespace App\Http\Controllers;

use App\Comment;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['data']]);
    }

    public function store(Request $request)
    {

        $user = auth()->user();
        $post_id = $request->id_post;
        $comment = $request->comment;

        $data = new Comment;
        $data->post_id = $post_id;
        $data->user_id = $user->id;
        $data->comment = $comment;
        $data->save();

        Session::flash("flash_data", [
            "id_post"     => "$post_id",
            "type"      => "is_comment"
        ]);

        alert()->success('Berhasil Komentar !!', 'Sukses');
        return redirect()->back();
    }

    public function data(Request $request)
    {
        $id = $request->id;
        $data = DB::table('comments as a')
                ->where('post_id',$id)
                ->leftJoin('users as b','a.user_id','=','b.id')
                ->select('a.id','a.comment','a.post_id','b.name','a.updated_at')
                ->orderBy('a.updated_at', 'ASC');

        return Datatables::of($data)
        ->addColumn('commentSpan', function($data) {
            return view('span.item-comment',[
                'data' => $data,
            ]);
        })->make(true);
    }
}
