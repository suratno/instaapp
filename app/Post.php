<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'post','poto','user_id'
    ];

    public function getTotalLikesAttribute()
    {
        return $this->hasMany('App\Like')->where('post_id',$this->id)->count();

    }

    public function getTotalCommentsAttribute()
    {
        return $this->hasMany('App\Comment')->where('post_id',$this->id)->count();

    }
}
