<div class="card">
    <div class="card-header">
        <h2>{{ $data->name }}</h2>
    </div>
    <div class="card-body">
        <blockquote class="blockquote mb-0">
            {{ $data->comment }}
            <footer class="blockquote-footer">{{ date("H:i d M Y", strtotime($data->updated_at)) }}</footer>
        </blockquote>
    </div>
</div>
