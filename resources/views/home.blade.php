@extends('layouts.app')

@section('content')
<main role="main">
    <section class="jumbotron text-center bg-secondary mb-0">
        <div class="container">
            <h1 class="jumbotron-heading">Insta Web App </h1>
             <p class="lead">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
            <p>
                <a href="#" class="btn btn-primary my-2">Main call to action</a>
            </p>
        </div>
    </section>

    <div class="album bg-dark">
        <div class="container">
            <div class="row">
                @if (Auth::check())
                <div class="col-md-12">
                    <div class="card mt-3 mb-3">

                        <div class="card-header bg-secondary text-white">
                            <h5 class="card-title">New Status</h5>
                        </div>

                        <div class="card-body">
                            <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="text" name="post" class="form-control{{ $errors->has('issued_by') ? ' is-invalid' : '' }} mt-2" value="{{ old('post') }}" required placeholder="Write Status">
                                        @if ($errors->has('post'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('post') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-5">
                                        <div class="custom-file mt-2">
                                            <input type="file" class="custom-file-input" name="photo" required id="customFile">
                                            <label class="custom-file-label form-control mb-2 mr-sm-2" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <button type="submit" class="btn btn-secondary mb-2">Submit</button>
                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>

                </div>
                @endif
            </div>

            <div class="row mt-3">

                @foreach ($data as $item)

                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top" src="{{ asset('img/'.$item->poto) }}" data-holder-rendered="true">
                        <div class="card-body">
                            <p class="card-text">{{ $item->post }}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ route('like') }}?idpost={{$item->id}}">
                                        <button type="button" class="btn btn-sm btn-outline-primary"> {{ $item->total_likes }} Like</button>
                                    </a>
                                    <button type="button" class="btn btn-sm btn-outline-success"
                                        data-toggle="modal" data-target="#modal-comment" data-id="{{$item->id}}"> {{ $item->total_comments }} Comment</button>
                                    @if (Auth::check())
                                        @if ( Auth::user()->id == $item->user_id)
                                            <button type="button" class="btn btn-sm btn-outline-secondary"> Edit</button>
                                            <button type="button" class="btn btn-sm btn-outline-danger"
                                                data-toggle="modal" data-target="#modal-delete" data-id="{{$item->id}}">
                                                Hapus
                                            </button>
                                        @endif
                                    @endif

                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach




            </div>
            <div class="row">
                {{ $data->links() }}
            </div>


        </div>
    </div>
</main>

@include('modal.delete')
@include('modal.comment')

<footer class="bg-secondary text-white pb-3 pt-3">
    <div class="container">
      <p class="float-right">
            <a href="#">Back to top</a>
      </p>
      <p>Album example is © Bootstrap, but please download and customize it for yourself!</p>
      <p>New to Bootstrap? <a href="../../">Visit the homepage</a> or read our <a href="../../getting-started/">getting started guide</a>.</p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('asset-web/datatables/DataTables-1.10.21/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset-web/datatables/DataTables-1.10.21/js/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset-web/datatables/FixedHeader-3.1.7/js/dataTables.fixedHeader.js') }}"></script>
<script type="text/javascript" src="{{ asset('asset-web/datatables/Responsive-2.2.4/js/dataTables.responsive.js') }}"></script>

<script>
    $(document).ready(function () {
        bsCustomFileInput.init()

        var commentTable = $('#table-comment').DataTable({
        pocessing: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        paging: false,
        ajax: {
            url: "{{ route('comment.data') }}",
            data: function (d) {
                d.id = $('#modal-comment').find('input[name=id]').val();
            }
        },
        columns: [

            {
                data: 'commentSpan',
                name: 'a.update_at',
                orderable: false,
            }

        ]
    })

        @if (session()->has('flash_data'))

            @if( session()->get('flash_data.type') == 'is_comment')
                var modal = $('#modal-comment')
                var id = "{{session()->get('flash_data.id_post')}}"

                modal.find('input[name=id]').val(id);
                modal.find('input[name=id_post]').val(id);

                modal.modal('show')

                commentTable.draw();
            @endif

        @endif

        $('#modal-delete').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id     = button.data('id')

            var modal = $(this)

            modal.find('input[name=id_post]').val(id);
        })

        $('#modal-comment').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var modal = $(this)
            var id = button.data('id')

            modal.find('input[name=id]').val(id);
            modal.find('input[name=id_post]').val(id);

            commentTable.draw();

        });
    })
</script>

@endsection
