<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Konfirmasi Hapus Status</h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('post.destroy') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row d-flex justify-content-center">
                        <h4 class="lead">Yakin ingin menghapus status ?</h4>
                        <input type="hidden" name="id_post" value="{{ old('id_request') }}" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn bg-danger">Konfirmasi Hapus</button>
                </div>
            </form>

        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
