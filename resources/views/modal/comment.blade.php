<div class="modal fade" id="modal-comment">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">List Comment</h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" value="" readonly />
                        <table id="table-comment" width="100%">
                            <thead>
                                <td width="100%"></td>
                            </thead>
                            <tbody >
                                <tr>
                                    <td>


                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                    <div class="col-12">
                        <div class="card mt-4 mr-4 ml-4 mb-3">
                            <form role="form" action="{{ route('comment.store') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id_post" value="" readonly />
                                <div class="card-body">

                                    <div class="form-group">
                                        <textarea class="form-control" name="comment" rows="2" placeholder="Write your comment"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>

                    </div>
                </div>

            </div>

        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
