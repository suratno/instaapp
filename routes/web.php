<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/post/store', 'PostController@store')->name('post.store');
Route::post('/post/destroy', 'PostController@destroy')->name('post.destroy');

Route::get('/like', 'LikeController@like')->name('like');

Route::post('/comment/store', 'CommentController@store')->name('comment.store');
Route::get('/comment/data', 'CommentController@data')->name('comment.data');
